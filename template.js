(function(){

	let divPalavrasEncontradas = document.querySelector('.container .palavrasEncontradas');
	let divPalavrasForm = document.querySelectorAll('.container .palavrasEncontradas .palavrasEncontradas_palavras, .container .palavrasEncontradas .form');

	document.addEventListener('DOMContentLoaded', (event) => {
		setTimeout(function(){
			divPalavrasEncontradas.classList.add('openPalavrasEncontradas');

			setTimeout(function(){
				divPalavrasForm.forEach(function(item){
					item.classList.add('showPalavrasEncontradas');
				});
			}, 500);
		}, 300);
	});




}());

const gifs = {
	img: document.querySelector(".gif img"),
	div: document.querySelector(".gif"),
	count: 1,

	showGif: function(){
	
		this.img.src = "gifs/"+this.count+".gif";
		this.div.style.display = "block";
		if(this.count == 29){
			this.count = 1;
		}else{
			this.count++;
		}
		
	},

	hideGif: function(){
		this.div.style.display = "none";
		this.img.src = "";
	}
}


let letras = document.querySelectorAll(".container .cacaPalavra table tr td");
let countLetras = 0;
for(let i = 0; i< letras.length; i++){
	setTimeout(function(){
		letras[i].classList.add("opacidade");
	}, 100);
}