const video ={
    url:  "http://hcdesenvolvimentos.com/videos/",
    estencao: ".mp4",
    nome: "",
    template: document.querySelector(".popUpVideo"),
    tagVideo: document.querySelector("video"),

    setNomeVideo: function(nome){
        this.nome = nome;
        video.getPopUpVideo();
    },

    getPopUpVideo: function(){
        this.template.classList.add("showVideo")
        this.tagVideo.src = this.url+this.nome+this.estencao;
        this.tagVideo.play()
        
    },

    fecharVideo: function(){
        this.template.classList.remove("showVideo")
        this.tagVideo.src = "";
        this.tagVideo.pause()
    }
}