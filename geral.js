


const cacaPalavra = {
    palavras: ["samba","abraco","gratidao","amizade","cerveja","conquista","coracao","deus","djavan", "familia","felicidade","basquete","happy","humildade","incrivel","orgulho","referencia","saudade","sonho","sucesso","vida","alegria","parceiro","querido","fim","parabens","colegio","bonus","bonus2","linda"],
    palavraAtual: "",
    palavrasEncontradas: [],
    btnSearchText: document.getElementById("btnSearchText"),

    /**
     * AÇÕES DE BUTTON E INPUT TEXT E ATTR PALAVRA ATUAL;
     */

    verificaPalavraAtual: function(){
        let searchText = this.btnSearchText.value;
        searchText = searchText.trim();
        searchText = searchText.toLowerCase();
        searchText = searchText.split(" ").join("-");
        searchText = searchText.split("  ").join("-");
        cacaPalavra.setPalavraAtual(searchText);
        if(this.palavras.indexOf(searchText)>-1){
            cacaPalavra.setPalavrasEncontradas(searchText)
            cacaPalavra.setListaTemplatePalavraEncontradas()
            cacaPalavra.limparCampo();
        }else{
            cacaPalavra.notificacoesDeError();
            cacaPalavra.notificaCampoVazio(true)
        }
    },

   setPalavraAtual: function(searchText){
        this.palavraAtual = searchText;
   },

   notificaCampoVazio: function(validacao){
        let btnSearch = this.btnSearchText;
        if(validacao){
            btnSearch.classList.add("validar");
        }else{
            btnSearch.classList.remove("validar");
        }
   },

   limparCampo: function(){
        this.btnSearchText.value = "";
   },

   /**
     * AÇÕES DE BUTTON E INPUT TEXT PALAVRAS ENCONTRADAS;
     */
    setPalavrasEncontradas: function(palavra){
        if(this.palavrasEncontradas.indexOf(palavra) == -1){
         this.palavrasEncontradas.push(palavra);
        }
    },
    
    setListaTemplatePalavraEncontradas: function(){
        let palavrasEncontradas_palavras = document.querySelector(".palavrasEncontradas_palavras");
        document.querySelector(".palavrasEncontradas_palavras ol").remove();
       
        let ol =  document.createElement("ol");
        palavrasEncontradas_palavras.appendChild(ol);
        let listaPalavrasEncontradas = document.querySelector(".palavrasEncontradas_palavras ol");
        let palavraFormatada = "";
        
        for(let i = 0;i<this.palavrasEncontradas.length;i++){
           
            let li = document.createElement("li");
            palavraFormatada = this.palavrasEncontradas[i];
           
            palavraFormatada = palavraFormatada.toUpperCase();
            palavraFormatada = palavraFormatada.split("-").join(" ");
            palavraFormatada = palavraFormatada.split(" ").join("  ");

            li.innerText = i+"-"+palavraFormatada;
            listaPalavrasEncontradas.appendChild(li);

        }

        cacaPalavra.notificacoesDeSucesso();
    },


    notificacoesDeSucesso: function(){
        let palavra = this.palavraAtual
    
        gifs.showGif();
        
        let successMessage = document.querySelector('.container .palavrasEncontradas .success-message');
        successMessage.innerText = "BOA PIAZINHO"
        successMessage.classList.add('show-message');
        setTimeout(function(){
            successMessage.innerText = "CERTÔ MIZERAVE"  
            setTimeout(function(){
                successMessage.classList.remove('show-message');
                gifs.hideGif();
                setTimeout(function(){
                    video.setNomeVideo(palavra);
                }, 500); 
            }, 3000); 
        }, 3000);    
    },
    notificacoesDeError: function(){
        let errorMessage = document.querySelector('.container .palavrasEncontradas .error-message');
        errorMessage.classList.add('show-message');

        if(cacaPalavra.palavraAtual != ""){
            errorMessage.innerHTML = "Véi nada a ve, <strong>"+cacaPalavra.palavraAtual+"</strong> não existe cara!"
        }else{
            errorMessage.innerHTML = "Poxa Rehnan, <strong>o campo ta vazio</strong> cara!"
        }
       setTimeout(function(){
            errorMessage.classList.remove('show-message');
        }, 3000);
        return false;
    }
    
}








